package ru.usetech.factorials;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {

//        задание числа, факториал которого должен находиться
        int n = 20;

//        выполнение функции расчета факториала
        String fact = factorial(n);

//        вывод результата вычисления факториала на консоль
        System.out.println("Factorial of " + n + " is : " + fact);
    }

//    функция расчета факториала
    public static String factorial(int n) {
        BigInteger fact = new BigInteger("1");
        for (int i = 1; i <= n; i++) {
            fact = fact.multiply(new BigInteger(i + ""));
        }
        return fact.toString();
    }
}
